import Vue from "vue/dist/vue.esm.js";
import VueRouter from "vue-router";
import IndexComponent from "../components/IndexComponent.vue";
import RouteTest from "../components/RouteTest.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "indexComponent",
    component: IndexComponent,
  },
  {
    path: "/test",
    name: "routeTest",
    component: RouteTest,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
