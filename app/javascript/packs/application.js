import Vue from "vue/dist/vue.esm.js";
import App from "../app.vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";


document.addEventListener("DOMContentLoaded", () => {
  document.body.appendChild(document.createElement("app"));
  const app = new Vue({
    el: "app",
    template: "<app/>",
    Vuetify,
    components: { App },
  });

  console.log(app);
});
